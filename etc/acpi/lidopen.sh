#!/bin/sh
. /etc/acpi/acpi.env #Source env
. /etc/acpi/acpi.funcs #source functions

sleep 0.6 #Sleep so I know if acpid is working  (this will add a slight delay so you can see all of the stuffs being restored)

pidstate "${MUSIC_PLAYER}" CONT #resume music player
pidstate "${MISC}" CONT #resume conky or misc things

"${BACKLIGHT}" -n "$(cat /tmp/backlight.acpid)" #Restore brightness

"${CPUMED}" #Set CPU near max so my battery doesn't die as fast
